package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
    public double getBookPrice(String temperature) {
        double t = Double.parseDouble(temperature);
        return (t * 1.8) + 32.0;
    }
}
